package com.powerhouse.meterreadings;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAutoConfiguration
public class MeterreadingsApplication {

	public static void main(String[] args) {
		SpringApplication.run(MeterreadingsApplication.class, args);
	}
}

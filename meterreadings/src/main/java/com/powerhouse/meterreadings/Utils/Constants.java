package com.powerhouse.meterreadings.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Constants
{
    //public static final String FILE_PATH_FRACTIONS = "C:\\Projects\\meterreadings\\meterreadings\\src\\main\\resources\\fractions.xlsx";

    //public static final String FILE_PATH_METER_READINGS = "C:\\Projects\\meterreadings\\meterreadings\\src\\main\\resources\\meterReadings.xlsx";

    public static final String FILE_PATH_FRACTIONS = ".\\fractions.xlsx";

    public static final String FILE_PATH_METER_READINGS = ".\\meterReadings.xlsx";

    public static final String FRACTION_DOCUMENT_CODE = "F";

    public static final String METER_READING_DOCUMENT_CODE = "M";

    public static final List<String> FRACTION_EXCEL_COLUMN_NAMES = new ArrayList<String>(Arrays.asList("Id","Month", "Profile", "Fraction"));

    public static final List<String> METER_READING_EXCEL_COLUMN_NAMES = new ArrayList<String>(Arrays.asList("Id","ConnectionID", "Profile", "Month", "Meter Reading"));

    public static final Double Limit_Fraction= new Double(1);

    public static final int Fractions_Month_Column_Index_= 1;

    public static final int Fractions_Profile_Column_Index_= 2;

    public static final int Fractions_Fraction_Column_Index_= 3;

    public static final int Fractions_Id_Column_Index_= 0;

    public static final int Meter_Readings_Id_Column_Index_= 0;

    public static final int Meter_Readings_ConnectionId_Column_Index_= 1;

    public static final int Meter_Readings_Profile_Column_Index_= 3;

    public static final int Meter_Readings_Month_Column_Index_= 2;

    public static final int Meter_Readings_Reading_Column_Index_= 4;




}

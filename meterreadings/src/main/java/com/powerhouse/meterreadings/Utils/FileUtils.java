package com.powerhouse.meterreadings.Utils;

import com.powerhouse.meterreadings.model.Fraction;
import com.powerhouse.meterreadings.model.MeterReading;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;

import static com.powerhouse.meterreadings.Utils.Constants.*;
import static com.powerhouse.meterreadings.Utils.Constants.Meter_Readings_Profile_Column_Index_;
import static com.powerhouse.meterreadings.Utils.Constants.Meter_Readings_Reading_Column_Index_;


/**
 *  This class will be used to manage all excel generic operations
 */
public class FileUtils
{
    public static Sheet prepareExcelFileToBeRead(String document) throws IOException
    {
        FileInputStream excelFile = createWorkBook(document);
        Workbook workbook = new XSSFWorkbook(excelFile);
        Sheet sheet = workbook.getSheetAt(0);
        return sheet;

    }

    public static FileInputStream createWorkBook(String document) throws FileNotFoundException
    {
        FileInputStream excelFile;
        String filePath;

        if (document.equals(Constants.FRACTION_DOCUMENT_CODE))
        {
            filePath = Constants.FILE_PATH_FRACTIONS;
        }
        else
        {
            filePath = Constants.FILE_PATH_METER_READINGS;
        }

        File file = new File(filePath);
        if (!file.exists())
        {
            createExcel(document);
        }
        excelFile = new FileInputStream(new File(filePath));

        return excelFile;
    }

    public static void writeToExcel(Workbook workbook, String document)
    {
        try
        {
            String filePath = document.equals(Constants.FRACTION_DOCUMENT_CODE) ? Constants.FILE_PATH_FRACTIONS : Constants.FILE_PATH_METER_READINGS;
            FileOutputStream outputStream = new FileOutputStream(filePath);
            workbook.write(outputStream);
            workbook.close();
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public static void createExcel(String operation)
    {
        XSSFWorkbook workbook = new XSSFWorkbook();

        XSSFSheet sheet = workbook.createSheet("data");
        Object[][] data = prepareHeading(operation);

        int rowNum = 0;
        System.out.println("Creating excel");

        for (Object[] datatype : data)
        {
            Row row = sheet.createRow(rowNum++);
            int colNum = 0;
            for (Object field : datatype)
            {
                Cell cell = row.createCell(colNum++);
                if (field instanceof String)
                {
                    cell.setCellValue((String) field);
                }
                else if (field instanceof Double)
                {
                    cell.setCellValue((Double) field);
                }
                else if (field instanceof Integer)
                {
                    cell.setCellValue((Integer) field);
                }
            }
        }

        writeToExcel(workbook, operation);

    }

    public static Object[][] prepareHeading(String operation)
    {

        if(operation.equals(Constants.FRACTION_DOCUMENT_CODE))
        {
            Object[][] data = { { "Id", "Month", "Profile", "Fraction" },
                    { 1, "JAN", "A", 0.2 },
                    { 2, "FEB", "A", 0.1 },
                    { 3, "JAN", "B", 0.3 },
                    { 4, "MAR", "A", 0.1 },
                    { 5, "FEB", "B", 0.5 } };

            return data;
        }
        else
        {
            Object[][] data = { { "Id","ConnectionID", "Month", "Profile", "Meter Reading" },
                    { 1,"0001", "JAN", "A", 10 },
                    { 2,"0004", "JAN", "B", 8 },
                    { 3,"0004", "FEB", "B", 10 },
                    { 4,"0001", "FEB", "A", 12 },
                    { 5,"0001", "MAR", "A", 18 } };

            return data;
        }

    }

    public static Row createCell(Row row, String documentCode, Fraction fraction, MeterReading meterReading)
    {
        if(documentCode.equals(Constants.FRACTION_DOCUMENT_CODE))
        {
            row.createCell(Fractions_Id_Column_Index_).setCellValue(fraction.getId());
            row.createCell(Fractions_Month_Column_Index_).setCellValue(fraction.getMonthName());
            row.createCell(Fractions_Profile_Column_Index_).setCellValue(fraction.getProfile());
            row.createCell(Fractions_Fraction_Column_Index_).setCellValue(fraction.getFraction());
        }
        else
        {
            row.createCell(Meter_Readings_Id_Column_Index_).setCellValue(meterReading.getId());
            row.createCell(Meter_Readings_ConnectionId_Column_Index_).setCellValue(meterReading.getConnectionId());
            row.createCell(Meter_Readings_Month_Column_Index_).setCellValue(meterReading.getMonth());
            row.createCell(Meter_Readings_Profile_Column_Index_).setCellValue(meterReading.getProfile());
            row.createCell(Meter_Readings_Reading_Column_Index_).setCellValue(meterReading.getMeterReading());
        }
        return row;
    }

    public static void addRowToExcel(Fraction fraction, String document, MeterReading meterReading) throws IOException
    {
        String documentCode = document.equals(Constants.FRACTION_DOCUMENT_CODE) ? Constants.FRACTION_DOCUMENT_CODE : Constants.METER_READING_DOCUMENT_CODE;
        Sheet sheet = prepareExcelFileToBeRead(documentCode);
        int lastRowNum = sheet.getLastRowNum();
        Row row = sheet.createRow(lastRowNum + 1);
        row = createCell(row, documentCode, fraction, meterReading);

        writeToExcel(sheet.getWorkbook(), documentCode);
    }

    public static void deleteRowOnExcel(int id, String document) throws IOException
    {
        Sheet sheet = prepareExcelFileToBeRead(document);

        //Retrieve the row and check for null
        Row row = sheet.getRow(id);

        //remove Row
        if (row != null)
        {
            sheet.removeRow(row);
            writeToExcel(sheet.getWorkbook(),document);
        }
    }
}

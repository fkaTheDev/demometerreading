package com.powerhouse.meterreadings.Utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;

public class RestUtils
{
    private static final Logger log = LoggerFactory.getLogger(RestUtils.class);

    private RestUtils() {
    }

    public static HttpHeaders createAlert(String message, String param) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("X-PowerHouse-alert", message);
        headers.add("X-PowerHouse-params", param);
        return headers;
    }

    public static HttpHeaders createFailureAlert(String entityName, String defaultMessage) {
        log.error("Entity processing failed, {}", defaultMessage);
        HttpHeaders headers = new HttpHeaders();
        headers.add("X-PowerHouse-error", defaultMessage);
        headers.add("X-PowerHouse-params", entityName);
        return headers;
    }

}

package com.powerhouse.meterreadings.Utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.powerhouse.meterreadings.model.Fraction;
import com.powerhouse.meterreadings.model.MeterReading;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;

import java.util.ArrayList;
import java.util.List;

public class UnitTestUtils
{
    private static final Logger log = LoggerFactory.getLogger(UnitTestUtils.class);

    private UnitTestUtils() {
    }

    public static List<MeterReading> meterBuilderByProfile(String profile, String connectionId)
    {
        List<MeterReading> meterReadingList = new ArrayList<>();

        meterReadingList.add(MeterReading.preapreMeterReading(1,connectionId,profile,"JAN",  50));
        meterReadingList.add(MeterReading.preapreMeterReading(2,connectionId,profile,"FEB", 150));
        meterReadingList.add(MeterReading.preapreMeterReading(3,connectionId,profile,"MAR", 250));
        meterReadingList.add(MeterReading.preapreMeterReading(4,connectionId,profile,"APR", 350));
        meterReadingList.add(MeterReading.preapreMeterReading(5,connectionId,profile,"MAY",  450));
        meterReadingList.add(MeterReading.preapreMeterReading(6,connectionId,profile,"JUN",  550));
        meterReadingList.add(MeterReading.preapreMeterReading(7,connectionId,profile,"JUL",  650));
        meterReadingList.add(MeterReading.preapreMeterReading(8,connectionId,profile,"AUG",  750));
        meterReadingList.add(MeterReading.preapreMeterReading(9,connectionId,profile,"SEP",  850));
        meterReadingList.add(MeterReading.preapreMeterReading(10,connectionId,profile,"OCT",  950));
        meterReadingList.add(MeterReading.preapreMeterReading(11,connectionId,profile,"NOV",  1050));
        meterReadingList.add(MeterReading.preapreMeterReading(12,connectionId,profile,"DEC",  1150));

        return meterReadingList;
    }

    public static List<MeterReading> meterBuilderByAll()
    {
        List<MeterReading> meterReadingList = meterBuilderByProfile("A", "0001");
        meterReadingList.addAll(meterBuilderByProfile("B", "0002"));

        return meterReadingList;
    }

    public static List<MeterReading> meterBuilderForValidation(String profile, String connectionId)
    {
        List<MeterReading> meterReadingList = new ArrayList<>();

        meterReadingList.add(MeterReading.preapreMeterReading(1,connectionId,profile,"JAN",  700));
        meterReadingList.add(MeterReading.preapreMeterReading(2,connectionId,profile,"FEB", 150));
        meterReadingList.add(MeterReading.preapreMeterReading(2,connectionId,profile,"FEB", 500));

        return meterReadingList;
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static List<Fraction> fractionBuilderByProfile(String profile)
    {
        List<Fraction> fractionList = new ArrayList<>();

        fractionList.add(Fraction.prepareFraction(1,"JAN", profile, new Double(0.1)));
        fractionList.add(Fraction.prepareFraction(2,"FEB", profile, new Double(0.05)));
        fractionList.add(Fraction.prepareFraction(3,"MAR", profile, new Double(0.05)));
        fractionList.add(Fraction.prepareFraction(4,"APR", profile, new Double(0.05)));
        fractionList.add(Fraction.prepareFraction(5,"MAY", profile, new Double(0.05)));
        fractionList.add(Fraction.prepareFraction(6,"JUN", profile, new Double(0.1)));
        fractionList.add(Fraction.prepareFraction(7,"JUL", profile, new Double(0.1)));
        fractionList.add(Fraction.prepareFraction(8,"AUG", profile, new Double(0.1)));
        fractionList.add(Fraction.prepareFraction(9,"SEP", profile, new Double(0.1)));
        fractionList.add(Fraction.prepareFraction(10,"OCT", profile, new Double(0.1)));
        fractionList.add(Fraction.prepareFraction(11,"NOV", profile, new Double(0.1)));
        fractionList.add(Fraction.prepareFraction(12,"DEC", profile, new Double(0.1)));

        return fractionList;
    }

    public static List<Fraction> fractionBuilderByAll()
    {
        List<Fraction> fractions = fractionBuilderByProfile("A");
        fractions.addAll(fractionBuilderByProfile("B" ));

        return fractions;
    }


}

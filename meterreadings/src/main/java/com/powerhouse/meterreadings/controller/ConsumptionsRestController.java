package com.powerhouse.meterreadings.controller;

import com.powerhouse.meterreadings.model.MeterReading;
import com.powerhouse.meterreadings.service.MeterReadingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@EnableAutoConfiguration
@RequestMapping("/api")
public class ConsumptionsRestController
{
    private final Logger log = LoggerFactory.getLogger(ConsumptionsRestController.class);

    @Autowired(required = true)
    private MeterReadingService meterReadingService;

    /**
     * GET  /consumption/{profile} : get all consumption values from excel file to prepare customer bill
     *
     * @param, no param
     * @return the ResponseEntity with status 200 (OK) and with body all users
     */
    @GetMapping("/consumption/{profile}")
    public List<MeterReading> getConsumptionByProfile(@PathVariable("profile") String profile) throws IOException
    {
        log.debug("REST request to get consumptions: {}");
        return this.meterReadingService.getMeterReadingsByProfile(profile);
    }

    /**
     * GET  /consumption/{profile}/{month} : get all consumption values from excel file to prepare customer bill
     *
     * @param, no param
     * @return the ResponseEntity with status 200 (OK) and with body all users
     */
    @GetMapping("/consumption/{profile}/{month}")
    public MeterReading getConsumptionByProfileAndMonth(@PathVariable("profile") String profile, @PathVariable("month") String month) throws IOException
    {
        log.debug("REST request to get Meter Readings: {}", profile);
        return this.meterReadingService.getMeterReadingsByProfileAndMonth(profile, month);
    }

}

package com.powerhouse.meterreadings.controller;

import com.powerhouse.meterreadings.Utils.RestUtils;
import com.powerhouse.meterreadings.model.Fraction;
import com.powerhouse.meterreadings.service.FractionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import static com.powerhouse.meterreadings.Utils.Constants.Limit_Fraction;

@RestController
@RequestMapping("/api")
public class FractionsRestController
{
	private final Logger log = LoggerFactory.getLogger(FractionsRestController.class);

	@Autowired(required = true)
	private FractionService fractionService;

	/**
	 * GET  /fractions : get all fractions values from excel file
	 *
	 * @param, no param
	 * @return the ResponseEntity with status 200 (OK) and with body all users
	 */
	@GetMapping("/fractions")
	public List<Fraction> getFractions() throws IOException
	{
		log.debug("REST request to get All Fractions: {}");
		return this.fractionService.getAllFractions();
	}

	/**
	 * GET  /fractions/{profile} : get all fractions values by profile from excel file
	 *
	 * @param, no param
	 * @return the ResponseEntity with status 200 (OK) and with body all users
	 */
	@GetMapping("/fractions/{profile}")
	public List<Fraction> getFractionByProfile(@PathVariable("profile") String profile) throws IOException
	{
		log.debug("REST request to get Fractions: {}", profile);
		return this.fractionService.getFractionsByProfile(profile);
	}

	/**
	 * POST  /addFraction  : Creates a new Fraction row and added it to the Excel File
	 * <p>
	 *
	 * @param fraction the fraction to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new meter reading, or with status 400 (Bad Request)
	 * @throws URISyntaxException if the Location URI syntax is incorrect
	 */
	@PostMapping("/addFraction")
	public ResponseEntity addFraction(@Valid @RequestBody Fraction fraction) throws URISyntaxException, IOException
	{
		log.debug("REST request to add fraction : {}", fraction);

		//check sum of all fractions whether it is bigger than 1 or not
		if (this.fractionService.getProfileFractionSum(fraction.getProfile()).compareTo(Limit_Fraction) == 1)
		{
			return ResponseEntity.badRequest()
					.headers(RestUtils.createFailureAlert("Fraction Management", "Limit has been reached. It must be less than 1")).body(null);
		}
		//save the fraction
		else
		{
			log.debug("REST request to add fraction : {}", fraction);

			this.fractionService.addFraction(fraction);

			return ResponseEntity.created(new URI("/api/addFraction/" + fraction.getProfile()))
					.headers(RestUtils.createAlert("A fraction has been added , Profile : " + fraction.getProfile(), fraction.getMonthName())).body(fraction);
		}

	}

	/**
	 * PUT  /updateMeterReading : Updates an existing fraction on Excel File.
	 *
	 * @param fraction to update
	 * @return the ResponseEntity with status 200 (OK),
	 * or with status 400 (Bad Request) ,
	 * or with status 500 (Internal Server Error)
	 */
	@PutMapping("/updateFraction")
	public ResponseEntity updateFraction(@Valid @RequestBody Fraction fraction) throws URISyntaxException, IOException
	{
		log.debug("REST request to update fraction : {}", fraction);

		//check sum of all fractions whether it is bigger than 1 or not
		if (this.fractionService.getProfileFractionSum(fraction.getProfile()).compareTo(Limit_Fraction) == 1)
		{
			return ResponseEntity.badRequest()
					.headers(RestUtils.createFailureAlert("Fraction Management", "Limit has been reached. It must be less than 1"))
					.body(null);
		}
		//save the fraction
		else
		{
			log.debug("REST request to update fraction : {}", fraction);
			this.fractionService.updateFraction(fraction);
			return ResponseEntity.created(new URI("/api/updateFraction/" + fraction.getProfile()))
					.headers(RestUtils.createAlert( "A fraction has been updated , Profile : " + fraction.getProfile(), fraction.getMonthName()))
					.body(fraction);

		}
	}

	/**
	 * DELETE /deleteFraction/{profile}/{monthName} : delete the "fraction".
	 *
	 * @param profile and month
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/deleteFraction/{profile}/{monthName}")
	public ResponseEntity deleteFraction(@PathVariable("profile") String profile, @PathVariable("monthName") String month) throws IOException
	{
		log.debug("REST request to delete fraction : {}", profile, month);

		this.fractionService.deleteFraction(profile, month);

		return ResponseEntity.ok().headers(RestUtils.createAlert( "A fraction is deleted with identifier " + profile, month)).build();

	}


}

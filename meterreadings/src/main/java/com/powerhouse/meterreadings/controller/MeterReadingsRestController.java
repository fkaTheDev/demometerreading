package com.powerhouse.meterreadings.controller;

import com.powerhouse.meterreadings.Utils.RestUtils;
import com.powerhouse.meterreadings.model.MeterReading;
import com.powerhouse.meterreadings.service.FractionService;
import com.powerhouse.meterreadings.service.MeterReadingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping("/api")
public class MeterReadingsRestController
{
    private final Logger log = LoggerFactory.getLogger(MeterReadingsRestController.class);

    @Autowired(required = true)
    private MeterReadingService meterReadingService;

    @Autowired(required = true)
    private FractionService fractionService;


    /**
     * GET  /meterReadings : get all meter readings values from excel file
     *
     * @param, no param
     * @return the ResponseEntity with status 200 (OK) and with body all users
     */
    @GetMapping("/meterReadings")
    public List<MeterReading> getAllMeterReadings() throws IOException
    {
        log.debug("REST request to get All Meter Readings: {}");
        return this.meterReadingService.getAllMeterReadings();
    }

    /**
     * POST  /addMeterReading  : Creates a new Meter Row taken from Customers and added it to the Excel File
     * <p>
     *
     * @param meterReading the meter to create
     * @return the ResponseEntity with status 201 (Created) and with body the new meter reading, or with status 400 (Bad Request)
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/addMeterReading")
    public ResponseEntity addMeterReading(@Valid @RequestBody MeterReading meterReading) throws URISyntaxException, IOException
    {
        log.debug("REST request to add meter Reading : {}", meterReading);

        // check the previous month whether it is lower or not
        if (this.meterReadingService.validateMeterReadingConsumption(meterReading))
        {
            return ResponseEntity.badRequest()
                    .headers(RestUtils.createFailureAlert("Meter Reading Management", "A meter reading for a month should not be lower than the previous one"))
                    .body(null);
        }
        //check the fractions profile if exist
        else if (this.fractionService.getFractionsByProfile(meterReading.getProfile()).size() < 1 )
        {
            return ResponseEntity.badRequest()
                    .headers(RestUtils.createFailureAlert("Meter Reading Management", "there should be data in the database for related profiles. Please add new profile"))
                    .body(null);
        }
        //add Meter Reading to Excel
        else
        {
            this.meterReadingService.addMeterReadings(meterReading);

            return ResponseEntity.created(new URI("/api/addMeterReading/" + meterReading.getProfile()))
                    .headers(RestUtils.createAlert( "A meter reading has been added successfully, Profile : " + meterReading.getProfile(), meterReading.getMonth()))
                    .body(meterReading);

        }
    }

    /**
     * PUT  /updateMeterReading : Updates an existing Meter Reading on Excel File.
     *
     * @param meterReading the meter Reading to update
     * @return the ResponseEntity with status 200 (OK),
     * or with status 400 (Bad Request) ,
     * or with status 500 (Internal Server Error)
     */
    @PutMapping("/updateMeterReading")
    public ResponseEntity updateMeterReading(@Valid @RequestBody MeterReading meterReading) throws URISyntaxException, IOException
    {
        log.debug("REST request to update meter reading : {}", meterReading);

        // check the previous month whether it is lower or not
        if (this.meterReadingService.validateMeterReadingConsumption(meterReading))
        {
            return ResponseEntity.badRequest()
                    .headers(RestUtils.createFailureAlert("Meter Reading Management", "A meter reading for a month should not be lower than the previous one"))
                    .body(null);
        }
        //check the fractions profile if exist
        else if (this.fractionService.getFractionsByProfile(meterReading.getProfile()).size() < 1 ) //check the fractions profile
        {
            return ResponseEntity.badRequest()
                    .headers(RestUtils.createFailureAlert("Meter Reading Management", "there should be data in the database for related profiles. Please add new profile"))
                    .body(null);
        }
        //upadte Meter Reading on Excel
        else
        {
            log.debug("REST request to update meter reading : {}", meterReading);
            this.meterReadingService.updateMeterReadings(meterReading);
            return ResponseEntity.created(new URI("/api/updateMeterReading/" + meterReading.getProfile()))
                    .headers(RestUtils.createAlert( "A meter reading has been updated successfully, Profile : " + meterReading.getProfile(), meterReading.getMonth()))
                    .body(meterReading);

        }
    }

    /**
     * DELETE /deleteMeterReading/{profile}/{month} : delete the "meter reading".
     *
     * @param profile and month
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/deleteMeterReading/{profile}/{month}")
    public ResponseEntity deleteMeterReading(@PathVariable("month") String month, @PathVariable("profile") String profile) throws IOException
    {
        log.debug("REST request to delete meter reading : {}", profile, month);

        this.meterReadingService.deleteMeterReadings(profile, month);

        return ResponseEntity.ok().headers(RestUtils.createAlert( "A meter reading is deleted with identifier profile  : %d , month : %d", profile)).build();

    }

    /**
     * GET  /validate : validate all data in the excel regarding to validations rule given by the Powerhouse and delete if there is a non acceptable data
     *
     * @param, no param
     * @return the ResponseEntity with status 200 (OK) and with body all users
     */
    @GetMapping("/validate")
    public List<MeterReading> validateAllMeterReadings() throws IOException
    {
        log.debug("REST request to validate All Meter Readings: {}");
        return this.meterReadingService.validateAllConsumption();
    }


}

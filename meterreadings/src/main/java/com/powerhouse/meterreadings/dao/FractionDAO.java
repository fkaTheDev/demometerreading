package com.powerhouse.meterreadings.dao;

import com.powerhouse.meterreadings.model.Fraction;

import java.io.IOException;
import java.util.List;

public interface FractionDAO
{
    public List<Fraction> getAllFractions() throws IOException;

    public List<Fraction> getFractionsByProfile(String profile) throws IOException;

    public void addFraction(Fraction fraction) throws IOException;

    public void updateFraction(Fraction fraction) throws IOException;

    public void deleteFraction(int fractionId) throws IOException;
}

package com.powerhouse.meterreadings.dao;

import com.powerhouse.meterreadings.model.Fraction;
import com.powerhouse.meterreadings.model.MeterReading;

import java.io.IOException;
import java.util.List;

public interface MeterReadingDAO
{
    public List<MeterReading> getAllMeterReadings() throws IOException;

    public List<MeterReading> getMeterReadingByProfile(String profile) throws IOException;

    public void addMeterReading(MeterReading meterReading) throws IOException;

    public void updateMeterReading(MeterReading meterReading) throws IOException;

    public void deleteMeterReading(int id) throws IOException;

}

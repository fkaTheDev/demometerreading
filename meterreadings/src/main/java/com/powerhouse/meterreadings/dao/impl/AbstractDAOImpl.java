package com.powerhouse.meterreadings.dao.impl;

import com.powerhouse.meterreadings.Utils.Constants;
import com.powerhouse.meterreadings.Utils.FileUtils;
import com.powerhouse.meterreadings.model.Fraction;
import com.powerhouse.meterreadings.model.MeterReading;
import org.apache.poi.ss.usermodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static com.powerhouse.meterreadings.Utils.Constants.*;

public class AbstractDAOImpl
{
    private final Logger log = LoggerFactory.getLogger(AbstractDAOImpl.class);

    /**
     *  It is used to prepare Fraction list to be used within the program by reading from Excel File
     */
    public List<Fraction> readFractionsFromExcel() throws IOException
    {
        List<Fraction> fractions = new ArrayList<>();

        Iterator<Row> iterator = FileUtils.prepareExcelFileToBeRead(Constants.FRACTION_DOCUMENT_CODE).iterator();

        while (iterator.hasNext())
        {
            Fraction fraction = new Fraction();
            Row currentRow = iterator.next();
            Iterator<Cell> cellIterator = currentRow.iterator();

            while (cellIterator.hasNext())
            {
                fractions = prepareFranctionList(cellIterator, fraction, fractions);
            }

            if (fraction.getMonthName() != null && fraction.getFraction() != null && fraction.getProfile() != null)
            {
                fractions.add(fraction);
            }
        }

        return fractions;

    }

    /**
     * It is used to prepare Meter Readings list to be used within the program by reading from Excel File
     */
    public List<MeterReading> readMeterReadingsFromExcel() throws IOException
    {
        List<MeterReading> meterReadings = new ArrayList<>();

        Iterator<Row> iterator = FileUtils.prepareExcelFileToBeRead(Constants.METER_READING_DOCUMENT_CODE).iterator();

        //fill the excel and Object List
        while (iterator.hasNext())
        {
            MeterReading meterReading = new MeterReading();
            Row currentRow = iterator.next();
            Iterator<Cell> cellIterator = currentRow.iterator();

            while (cellIterator.hasNext())
            {
                meterReadings = prepareMeterReadingList(cellIterator, meterReading, meterReadings);
            }

            if (meterReading.getConnectionId() != null && meterReading.getMonth() != null && meterReading.getProfile() != null)
            {
                meterReadings.add(meterReading);
            }
        }

        return meterReadings;

    }


    /**
     * It is used to fill Meter Reading Fields to be used within the program by reading from Excel File
     */
    public List<MeterReading> prepareMeterReadingList(Iterator<Cell> cellIterator, MeterReading meterReading, List<MeterReading> meterReadings)
    {
        Cell currentCell = cellIterator.next();
        if (currentCell.getCellTypeEnum() == CellType.STRING)
        {
            if (!Constants.METER_READING_EXCEL_COLUMN_NAMES.contains(currentCell.getStringCellValue()))
            {
                if (currentCell.getColumnIndex() == Constants.Meter_Readings_Profile_Column_Index_)
                {
                    meterReading.setProfile(currentCell.getStringCellValue());
                }
                else if (currentCell.getColumnIndex() == Constants.Meter_Readings_Month_Column_Index_)
                {
                    meterReading.setMonth(currentCell.getStringCellValue());
                }
                else
                {
                    meterReading.setConnectionId(currentCell.getStringCellValue());
                }
            }

        }
        else if (currentCell.getCellTypeEnum() == CellType.NUMERIC)
        {
            if (currentCell.getColumnIndex() == Constants.Meter_Readings_Id_Column_Index_)
            {
                meterReading.setId(currentCell.getRowIndex());
            }
            else
            {
                meterReading.setMeterReading((int) currentCell.getNumericCellValue());
            }
        }

        return meterReadings;
    }


    /**
     *  It is used to fill Fraction Fields to be used within the program by reading from Excel File
     */
    public List<Fraction> prepareFranctionList(Iterator<Cell> cellIterator, Fraction fraction, List<Fraction> fractions)
    {
        Cell currentCell = cellIterator.next();
        if (currentCell.getCellTypeEnum() == CellType.STRING)
        {
            if (!Constants.FRACTION_EXCEL_COLUMN_NAMES.contains(currentCell.getStringCellValue()))
            {
                if (fraction.getMonthName() != null)
                {
                    fraction.setProfile(currentCell.getStringCellValue());
                }
                else
                {
                    fraction.setMonthName(currentCell.getStringCellValue());
                }
            }

        }
        else if (currentCell.getCellTypeEnum() == CellType.NUMERIC)
        {
            if(currentCell.getColumnIndex() == Fractions_Id_Column_Index_)
            {
                fraction.setId(currentCell.getRowIndex());
            }
            else
            {
                fraction.setFraction(currentCell.getNumericCellValue());
            }
        }

        return fractions;
    }


    /**
     * It is used to update Fractions on the Excel
     */
    public void updateFractionOnExcel(Fraction fraction) throws IOException
    {
        Sheet sheet = FileUtils.prepareExcelFileToBeRead(Constants.FRACTION_DOCUMENT_CODE);

        //Retrieve the row and check for null
        Row row = sheet.getRow(fraction.getId());
        if (row != null)
        {
            //Get the value of cell
            Cell cellId = row.getCell(Constants.Fractions_Id_Column_Index_);
            Cell cellMonth = row.getCell(Constants.Fractions_Month_Column_Index_);
            Cell cellProfile = row.getCell(Constants.Fractions_Profile_Column_Index_);
            Cell cellFraction = row.getCell(Constants.Fractions_Fraction_Column_Index_);

            //Update the value of cell
            cellId.setCellValue(fraction.getId());
            cellMonth.setCellValue(fraction.getMonthName());
            cellProfile.setCellValue(fraction.getProfile());
            cellFraction.setCellValue(fraction.getFraction());

            FileUtils.writeToExcel(sheet.getWorkbook(),Constants.FRACTION_DOCUMENT_CODE);
        }
    }

    /**
     * It is used to update Meter Readings on the Excel
    */
    public void updateMeterReadingsOnExcel(MeterReading meterReading) throws IOException
    {
        Sheet sheet = FileUtils.prepareExcelFileToBeRead(Constants.METER_READING_DOCUMENT_CODE);

        //Retrieve the row and check for null
        Row row = sheet.getRow(meterReading.getId());
        if (row != null)
        {
            //Get the value of cell
            Cell cellId = row.getCell(Constants.Meter_Readings_Id_Column_Index_);
            Cell cellconId = row.getCell(Constants.Meter_Readings_ConnectionId_Column_Index_);
            Cell cellMonth = row.getCell(Constants.Meter_Readings_Month_Column_Index_);
            Cell cellProfile = row.getCell(Constants.Meter_Readings_Profile_Column_Index_);
            Cell cellReading = row.getCell(Constants.Meter_Readings_Reading_Column_Index_);

            //Update the value of cell
            cellId.setCellValue(meterReading.getId());
            cellconId.setCellValue(meterReading.getConnectionId());
            cellMonth.setCellValue(meterReading.getMonth());
            cellProfile.setCellValue(meterReading.getProfile());
            cellReading.setCellValue(meterReading.getMeterReading());

            FileUtils.writeToExcel(sheet.getWorkbook(),Constants.METER_READING_DOCUMENT_CODE);
        }
    }


}

package com.powerhouse.meterreadings.dao.impl;

import com.powerhouse.meterreadings.Utils.Constants;
import com.powerhouse.meterreadings.Utils.FileUtils;
import com.powerhouse.meterreadings.dao.FractionDAO;
import com.powerhouse.meterreadings.model.Fraction;
import com.powerhouse.meterreadings.model.MeterReading;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Repository("fractionDAO")
public class FractionDAOImpl extends AbstractDAOImpl implements FractionDAO
{

    @Override
    public List<Fraction> getAllFractions() throws IOException
    {
        return this.readFractionsFromExcel();
    }

    @Override
    public List<Fraction> getFractionsByProfile(String profile) throws IOException
    {
        List<Fraction> fractions = this.readFractionsFromExcel();
        List<Fraction> fractionsByProfile = new ArrayList<>();

        for (Fraction fraction : fractions)
        {
            if (fraction.getProfile().equals(profile))
            {
                fractionsByProfile.add(fraction);
            }
        }

        return fractionsByProfile;
    }

    @Override
    public void addFraction(Fraction fraction) throws IOException
    {
        FileUtils.addRowToExcel(fraction, Constants.FRACTION_DOCUMENT_CODE, new MeterReading());
    }

    @Override
    public void updateFraction(Fraction fraction) throws IOException
    {
        this.updateFractionOnExcel(fraction);
    }

    @Override
    public void deleteFraction(int fractionId) throws IOException
    {
        FileUtils.deleteRowOnExcel(fractionId, Constants.FRACTION_DOCUMENT_CODE);
    }

}

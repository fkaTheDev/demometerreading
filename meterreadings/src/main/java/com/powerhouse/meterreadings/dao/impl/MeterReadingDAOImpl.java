package com.powerhouse.meterreadings.dao.impl;

import com.powerhouse.meterreadings.Utils.Constants;
import com.powerhouse.meterreadings.Utils.FileUtils;
import com.powerhouse.meterreadings.dao.FractionDAO;
import com.powerhouse.meterreadings.dao.MeterReadingDAO;
import com.powerhouse.meterreadings.model.Fraction;
import com.powerhouse.meterreadings.model.MeterReading;
import org.apache.xmlbeans.impl.jam.mutable.MElement;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Repository("meterReadingDAO")
public class MeterReadingDAOImpl extends AbstractDAOImpl implements MeterReadingDAO
{

    @Override
    public List<MeterReading> getAllMeterReadings() throws IOException
    {
        return readMeterReadingsFromExcel();
    }

    @Override
    public List<MeterReading> getMeterReadingByProfile(String profile) throws IOException
    {
        List<MeterReading> meterReadings = this.readMeterReadingsFromExcel();

        List<MeterReading> meterReadingsByProfile = new ArrayList<>();

        for (MeterReading meterReading : meterReadings)
        {
            if (meterReading.getProfile().equals(profile))
            {
                meterReadingsByProfile.add(meterReading);
            }
        }

        return meterReadingsByProfile;
    }

    @Override
    public void addMeterReading(MeterReading meterReading) throws IOException
    {
        FileUtils.addRowToExcel(new Fraction(),Constants.METER_READING_DOCUMENT_CODE, meterReading);
    }

    @Override
    public void updateMeterReading(MeterReading meterReading) throws IOException
    {
        this.updateMeterReadingsOnExcel(meterReading);
    }

    @Override
    public void deleteMeterReading(int id) throws IOException
    {
        FileUtils.deleteRowOnExcel(id, Constants.METER_READING_DOCUMENT_CODE);
    }


}

package com.powerhouse.meterreadings.model;

public class Fraction
{

    private int id;

    private String monthName;

    private String profile;

    private Double fraction;

    public Fraction()
    {

    }

    public Fraction(int id, String profile, String monthName, Double fraction)
    {
        this.id = id;
        this.monthName = monthName;
        this.profile = profile;
        this.fraction = fraction;
    }


    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getMonthName()
    {
        return monthName;
    }

    public void setMonthName(String monthName)
    {
        this.monthName = monthName;
    }

    public String getProfile()
    {
        return profile;
    }

    public void setProfile(String profile)
    {
        this.profile = profile;
    }

    public Double getFraction()
    {
        return fraction;
    }

    public void setFraction(Double fraction)
    {
        this.fraction = fraction;
    }

    public static Fraction prepareFraction(int id, String monthName, String profile, Double fractionValue)
    {
        Fraction fraction = new Fraction();
        fraction.setId(id);
        fraction.setMonthName(monthName);
        fraction.setProfile(profile);
        fraction.setFraction(fractionValue);
        return fraction;
    }

}

package com.powerhouse.meterreadings.model;

public class MeterReading
{

    private int id;

    private String connectionId;

    private String month;

    private String profile;

    private int meterReading;

    public MeterReading()
    {

    }

    public MeterReading(int id,String connectionId, String profile, String month, int meterReading)
    {
        this.id = id;
        this.connectionId = connectionId;
        this.month = month;
        this.profile = profile;
        this.meterReading = meterReading;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getConnectionId()
    {
        return connectionId;
    }

    public void setConnectionId(String connectionId)
    {
        this.connectionId = connectionId;
    }

    public String getMonth()
    {
        return month;
    }

    public void setMonth(String month)
    {
        this.month = month;
    }

    public String getProfile()
    {
        return profile;
    }

    public void setProfile(String profile)
    {
        this.profile = profile;
    }

    public int getMeterReading()
    {
        return meterReading;
    }

    public void setMeterReading(int meterReading)
    {
        this.meterReading = meterReading;
    }

    public static MeterReading preapreMeterReading(int id,String connectionId, String profile, String month, int meterReadingValue)
    {
        MeterReading meterReading = new MeterReading();
        meterReading.setId(id);
        meterReading.setConnectionId(connectionId);
        meterReading.setProfile(profile);
        meterReading.setMonth(month);
        meterReading.setMeterReading(meterReadingValue);
        return meterReading;
    }

}

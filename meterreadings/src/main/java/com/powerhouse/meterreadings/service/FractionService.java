package com.powerhouse.meterreadings.service;

import com.powerhouse.meterreadings.model.Fraction;

import java.io.IOException;
import java.util.List;

public interface FractionService
{

    public List<Fraction> getAllFractions() throws IOException;

    public List<Fraction> getFractionsByProfile(String profile) throws IOException;

    public Fraction getFractionByProfileAndMonth(String profile, String month) throws IOException;

    public Double getProfileFractionSum(String profile) throws IOException;

    public void addFraction(Fraction fraction) throws IOException;

    public void updateFraction(Fraction fraction) throws IOException;

    public void deleteFraction(String profile, String month) throws IOException;

}

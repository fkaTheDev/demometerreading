package com.powerhouse.meterreadings.service;

import com.powerhouse.meterreadings.dao.MeterReadingDAO;
import com.powerhouse.meterreadings.model.Fraction;
import com.powerhouse.meterreadings.model.MeterReading;

import java.io.IOException;
import java.util.List;

public interface MeterReadingService
{

    public List<MeterReading> getAllMeterReadings() throws IOException;

    public List<MeterReading> getMeterReadingsByProfile(String profile) throws IOException;

    public MeterReading getMeterReadingsByProfileAndMonth(String profile, String month) throws IOException;

    public Boolean validateMeterReadingConsumption(MeterReading meterReading) throws IOException;

    public void addMeterReadings(MeterReading meterReading) throws IOException;

    public void updateMeterReadings(MeterReading meterReading) throws IOException;

    public void deleteMeterReadings(String profile, String month) throws IOException;

    public List<MeterReading> validateAllConsumption() throws IOException;

}

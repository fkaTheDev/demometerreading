package com.powerhouse.meterreadings.service.impl;

import com.powerhouse.meterreadings.dao.FractionDAO;
import com.powerhouse.meterreadings.model.Fraction;
import com.powerhouse.meterreadings.service.FractionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

@Service
public class FractionServiceImpl implements FractionService
{
    @Autowired
    private FractionDAO fractionDAO;

    @PostConstruct
    void init()
    {
    }

    @Override
    public List<Fraction> getAllFractions() throws IOException
    {
        return this.fractionDAO.getAllFractions();
    }

    @Override
    public List<Fraction> getFractionsByProfile(String profile) throws IOException
    {
        return this.fractionDAO.getFractionsByProfile(profile);
    }

    @Override
    public Fraction getFractionByProfileAndMonth(String profile, String month) throws IOException
    {
        List<Fraction> fractionsByProfile = this.fractionDAO.getFractionsByProfile(profile);
        Fraction fractionResult = new Fraction();

        for (Fraction fraction : fractionsByProfile)
        {
             if (fraction.getMonthName().toUpperCase().equals(month.toUpperCase()))
             {
                 fractionResult = fraction;
             }
        }
        return fractionResult;
    }

    @Override
    public Double getProfileFractionSum(String profile) throws IOException
    {
        List<Fraction> fractionsByProfile = this.fractionDAO.getFractionsByProfile(profile);

        List<Double> consumptions = new ArrayList<>();

        for (Fraction fraction : fractionsByProfile)
        {
            consumptions.add(fraction.getFraction());
        }

        return consumptions.stream().mapToDouble(p -> p).sum();
    }

    @Override
    public void addFraction(Fraction fraction) throws IOException
    {
        fraction.setId(getLastFractionId() + 1);
        this.fractionDAO.addFraction(fraction);
    }

    @Override
    public void updateFraction(Fraction fraction) throws IOException
    {
        this.fractionDAO.updateFraction(fraction);
    }

    @Override
    public void deleteFraction(String profile, String month) throws IOException
    {
        int fractionId = findFractionId(profile, month);
        this.fractionDAO.deleteFraction(fractionId);
    }

    private int findFractionId(String profile, String month) throws IOException
    {
        List<Fraction> fractions = getFractionsByProfile(profile);
        int id = 0;
        for (Fraction fraction : fractions)
        {
            if (fraction.getProfile().equals(profile) && fraction.getMonthName().equals(month))
            {
                id = fraction.getId();
            }
        }
        return id;
    }

    private int getLastFractionId() throws IOException
    {
        return this.fractionDAO.getAllFractions().get(fractionDAO.getAllFractions().size() - 1).getId();
    }

}

package com.powerhouse.meterreadings.service.impl;

import com.powerhouse.meterreadings.dao.MeterReadingDAO;
import com.powerhouse.meterreadings.model.Fraction;
import com.powerhouse.meterreadings.model.MeterReading;
import com.powerhouse.meterreadings.service.FractionService;
import com.powerhouse.meterreadings.service.MeterReadingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.text.DateFormatSymbols;
import java.time.Month;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Service("meterReadingService")
public class MeterReadingServiceImpl implements MeterReadingService
{

    @Autowired
    public MeterReadingDAO meterReadingDAO;

    @Autowired
    private FractionService fractionService;

    @PostConstruct
    void init()
    {
    }

    @Override
    public List<MeterReading> getAllMeterReadings() throws IOException
    {
        return this.meterReadingDAO.getAllMeterReadings();
    }

    @Override
    public List<MeterReading> getMeterReadingsByProfile(String profile) throws IOException
    {
        return this.meterReadingDAO.getMeterReadingByProfile(profile);
    }

    @Override
    public MeterReading getMeterReadingsByProfileAndMonth(String profile, String month) throws IOException
    {
        MeterReading meterReadingResult = new MeterReading();
        List<MeterReading> meterReadings = meterReadingDAO.getMeterReadingByProfile(profile);

        for (MeterReading meterReading : meterReadings)
        {
            if (meterReading.getMonth().toUpperCase().equals(month.toUpperCase()))
            {

                meterReadingResult = meterReading;
            }
        }

        return meterReadingResult;
    }

    @Override
    public void addMeterReadings(MeterReading meterReading) throws IOException
    {
        meterReading.setId(getLastMeterReadingsId() + 1);
        this.meterReadingDAO.addMeterReading(meterReading);
    }

    @Override
    public void updateMeterReadings(MeterReading meterReading) throws IOException
    {
        this.meterReadingDAO.updateMeterReading(meterReading);
    }

    @Override
    public void deleteMeterReadings(String profile, String month) throws IOException
    {
        int id = findMeterReadingId(profile, month);
        this.meterReadingDAO.deleteMeterReading(id);
    }

    @Override
    public List<MeterReading> validateAllConsumption() throws IOException
    {
        //get the All Meter Readings
        List<MeterReading> meterReadings = meterReadingDAO.getAllMeterReadings();

        //calculate total Consumption
        int totalConsumption = calculateTheTotalConsumptionInaYear(meterReadings);
        List<MeterReading> meterReadingsDeleted = new ArrayList<>();

        for (MeterReading meterReading : meterReadings)
        {
            Fraction fraction = this.fractionService.getFractionByProfileAndMonth(meterReading.getProfile(), meterReading.getMonth());
            double estimatedValue = totalConsumption * fraction.getFraction();
            double estimatedValueHigher = (estimatedValue * 25 / 100) + estimatedValue;
            double estimatedValueLower = estimatedValue - (estimatedValue * 25 / 100);

            if (!(meterReading.getMeterReading() < estimatedValueHigher && estimatedValueLower < meterReading.getMeterReading())
                    || this.validateMeterReadingConsumption(meterReading)
                    || this.fractionService.getFractionsByProfile(meterReading.getProfile()).size() < 1) // check 3 validations (%25 rule, if profile exist or not, previous month rule)
            {
                meterReadingsDeleted.add(meterReading);
                this.deleteMeterReadings(meterReading.getProfile(), meterReading.getMonth());
            }
        }

        return meterReadingsDeleted;
    }


    @Override
    public Boolean validateMeterReadingConsumption(MeterReading meterReading) throws IOException
    {
        // it is used to check previous consumption is less than current consumption
        if (!meterReading.getMonth().toUpperCase().equals(Month.JANUARY.getDisplayName(TextStyle.SHORT, Locale.ENGLISH).toUpperCase()))
        {
            String previousMonth = getPreviousMonth(meterReading.getMonth());
            int previousMonthConsumption = getPreviousMonthConsumption(meterReading.getProfile(), previousMonth);
            return previousMonthConsumption >= meterReading.getMeterReading();
        }
        else
        {
            // if it is Jan, no need to control
            return false;
        }
    }

    private int getPreviousMonthConsumption(String profile, String previousMonth) throws IOException
    {
        MeterReading meterReading = getMeterReadingsByProfileAndMonth(profile , previousMonth);
        return meterReading.getMeterReading();
    }

    private String getPreviousMonth(String monthName)
    {
        String[] shortMonths = new DateFormatSymbols(Locale.ENGLISH).getShortMonths();
        int k = 0;
        for (int i = 0; i < (shortMonths.length-1); i++) {
            if (shortMonths[i].toUpperCase().equals(monthName.toUpperCase()))
            {
                k = i;
            }
        }

        return shortMonths[k - 1].toUpperCase();
    }

    private int getLastMeterReadingsId() throws IOException
    {
        return this.meterReadingDAO.getAllMeterReadings().get(meterReadingDAO.getAllMeterReadings().size() - 1).getId();
    }

    private int findMeterReadingId(String profile, String month) throws IOException
    {
        List<MeterReading> meterReadings = getMeterReadingsByProfile(profile);
        int id = 0;
        for (MeterReading meterReading : meterReadings)
        {
            if (meterReading.getProfile().toUpperCase().equals(profile.toUpperCase()) && meterReading.getMonth().toUpperCase().equals(month.toUpperCase()))
            {
                id = meterReading.getId();
            }
        }
        return id;
    }

    private int calculateTheTotalConsumptionInaYear(List<MeterReading> meterReadings)
    {
        int total = 0;
        for (MeterReading meterReading : meterReadings)
        {
            total = total + meterReading.getMeterReading();
        }

        return total;
    }

}

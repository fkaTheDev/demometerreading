package com.powerhouse.meterreadings.controller;

import com.powerhouse.meterreadings.MeterreadingsApplication;
import com.powerhouse.meterreadings.Utils.UnitTestUtils;
import com.powerhouse.meterreadings.model.MeterReading;
import com.powerhouse.meterreadings.service.MeterReadingService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.Mockito.*;
import static org.mockito.internal.verification.VerificationModeFactory.times;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest (classes = MeterreadingsApplication.class)
public class ConsumptionsRestControllerTest
{
    @MockBean
    private MeterReadingService meterReadingService;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void getConsumptionByProfileListSizeAsExpected() throws Exception
    {

        List<MeterReading> meterReadings = UnitTestUtils.meterBuilderByProfile("A", "0001");

        when(meterReadingService.getMeterReadingsByProfile("A")).thenReturn(meterReadings);

        mockMvc.perform(get("/api/consumption/A/"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$", hasSize(12)))
                .andDo(print())
                .andReturn();


        verify(meterReadingService, times(1)).getMeterReadingsByProfile("A");
        verifyNoMoreInteractions(meterReadingService);
    }

    @Test
    public void getConsumptionByProfileListIsNotFound() throws Exception
    {
        when(meterReadingService.getMeterReadingsByProfile("A")).thenReturn(null);

        mockMvc.perform(get("/api/wronglink/A/"))
                .andExpect(status().isNotFound())
                .andDo(print())
                .andReturn();
    }

    @Test
    public void getConsumptionByProfileAndMonthAsExpected() throws Exception
    {
        MeterReading meterReading = MeterReading.preapreMeterReading(1,"0001","A", "JAN", 50);

        when(meterReadingService.getMeterReadingsByProfileAndMonth("A", "JAN")).thenReturn(meterReading);

        mockMvc.perform(get("/api/consumption/A/JAN"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.connectionId").value("0001"))
                .andExpect(jsonPath("$.profile").value("A"))
                .andExpect(jsonPath("$.month").value("JAN"))
                .andExpect(jsonPath("$.meterReading").value(50))
                .andDo(print())
                .andReturn();

        verify(meterReadingService, times(1)).getMeterReadingsByProfileAndMonth("A", "JAN");
        verifyNoMoreInteractions(meterReadingService);
    }

    @Test
    public void getConsumptionByProfileAndMonthIsNotFound() throws Exception
    {
        when(meterReadingService.getMeterReadingsByProfileAndMonth("A", "JAN")).thenReturn(null);

        mockMvc.perform(get("/api/wronglink/A/JAN"))
                .andExpect(status().isNotFound())
                .andDo(print())
                .andReturn();

    }

    @Test
    public void getConsumptionByProfileAndMonthAsNotExpectedByConId() throws Exception
    {
        MeterReading meterReading = MeterReading.preapreMeterReading(1,"0001","A", "JAN", 50);
        meterReading.setConnectionId(null);

        when(meterReadingService.getMeterReadingsByProfileAndMonth("A", "JAN")).thenReturn(meterReading);

        mockMvc.perform(get("/api/consumption/A/JAN"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.connectionId").doesNotExist())
                .andExpect(jsonPath("$.profile").value("A"))
                .andExpect(jsonPath("$.month").value("JAN"))
                .andExpect(jsonPath("$.meterReading").value(50))
                .andDo(print())
                .andReturn();

        verify(meterReadingService, times(1)).getMeterReadingsByProfileAndMonth("A", "JAN");
        verifyNoMoreInteractions(meterReadingService);
    }

    @Test
    public void getConsumptionByProfileAndMonthAsNotExpectedByProfile() throws Exception
    {
        MeterReading meterReading = MeterReading.preapreMeterReading(1,"0001","A", "JAN", 50);
        meterReading.setProfile(null);

        when(meterReadingService.getMeterReadingsByProfileAndMonth("A", "JAN")).thenReturn(meterReading);

        mockMvc.perform(get("/api/consumption/A/JAN"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.connectionId").value("0001"))
                .andExpect(jsonPath("$.profile").doesNotExist())
                .andExpect(jsonPath("$.month").value("JAN"))
                .andExpect(jsonPath("$.meterReading").value(50))
                .andDo(print())
                .andReturn();

        verify(meterReadingService, times(1)).getMeterReadingsByProfileAndMonth("A", "JAN");
        verifyNoMoreInteractions(meterReadingService);
    }

    @Test
    public void getConsumptionByProfileAndMonthAsNotExpectedByMonth() throws Exception
    {
        MeterReading meterReading = MeterReading.preapreMeterReading(1,"0001","A", "JAN", 50);
        meterReading.setMonth(null);

        when(meterReadingService.getMeterReadingsByProfileAndMonth("A", "JAN")).thenReturn(meterReading);

        mockMvc.perform(get("/api/consumption/A/JAN"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.connectionId").value("0001"))
                .andExpect(jsonPath("$.profile").value("A"))
                .andExpect(jsonPath("$.month").doesNotExist())
                .andExpect(jsonPath("$.meterReading").value(50))
                .andDo(print())
                .andReturn();

        verify(meterReadingService, times(1)).getMeterReadingsByProfileAndMonth("A", "JAN");
        verifyNoMoreInteractions(meterReadingService);
    }

}
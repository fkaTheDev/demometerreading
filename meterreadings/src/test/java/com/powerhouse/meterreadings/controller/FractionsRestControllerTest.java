package com.powerhouse.meterreadings.controller;

import com.powerhouse.meterreadings.MeterreadingsApplication;
import com.powerhouse.meterreadings.Utils.UnitTestUtils;
import com.powerhouse.meterreadings.model.Fraction;
import com.powerhouse.meterreadings.model.MeterReading;
import com.powerhouse.meterreadings.service.FractionService;
import com.powerhouse.meterreadings.service.MeterReadingService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;

import static com.powerhouse.meterreadings.Utils.Constants.Limit_Fraction;
import static com.powerhouse.meterreadings.Utils.UnitTestUtils.asJsonString;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.mockito.internal.verification.VerificationModeFactory.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MeterreadingsApplication.class)
public class FractionsRestControllerTest
{

    @MockBean
    private FractionService fractionService;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    @Before public void setUp() throws Exception
    {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @After
    public void tearDown() throws Exception
    {

    }

    @Test
    public void getFractions() throws Exception
    {
        List<Fraction> fractions = UnitTestUtils.fractionBuilderByProfile("A");


        when(fractionService.getAllFractions()).thenReturn(fractions);

        mockMvc.perform(get("/api/fractions/"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andDo(print())
                .andReturn();


        verify(fractionService, times(1)).getAllFractions();
        verifyNoMoreInteractions(fractionService);
    }

    @Test public void getFractionByProfile() throws Exception
    {
        List<Fraction> fractions = UnitTestUtils.fractionBuilderByProfile("A");

        when(fractionService.getFractionsByProfile("A")).thenReturn(fractions);

        mockMvc.perform(get("/api/fractions/{profile}", "A"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$", hasSize(12)))
                .andDo(print())
                .andReturn();


        verify(fractionService, times(1)).getFractionsByProfile("A");
        verifyNoMoreInteractions(fractionService);
    }

    @Test public void addFraction() throws Exception
    {
        Fraction fraction = UnitTestUtils.fractionBuilderByProfile("A").get(0);

        when(fractionService.getProfileFractionSum(fraction.getProfile())).thenReturn(new Double(0.5));

        doNothing().when(fractionService).addFraction(fraction);

        mockMvc.perform(
                post("/api/addFraction/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(fraction)))
                .andExpect(status().isCreated())
                .andDo(print())
                .andReturn();
    }

    @Test public void addFractionFailed() throws Exception
    {
        Fraction fraction = UnitTestUtils.fractionBuilderByProfile("A").get(0);

        when(fractionService.getProfileFractionSum(fraction.getProfile())).thenReturn(new Double(2));

        doNothing().when(fractionService).addFraction(fraction);

        mockMvc.perform(
                post("/api/addFraction/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(fraction)))
                .andExpect(status().isBadRequest())
                .andDo(print())
                .andReturn();
    }

    @Test public void updateFraction() throws Exception
    {
        Fraction fraction = UnitTestUtils.fractionBuilderByProfile("A").get(0);

        when(fractionService.getProfileFractionSum(fraction.getProfile())).thenReturn(new Double(1));

        doNothing().when(fractionService).addFraction(fraction);

        mockMvc.perform(
                put("/api/updateFraction/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(fraction)))
                .andExpect(status().isCreated())
                .andDo(print())
                .andReturn();
    }

    @Test public void updateFractionFailed() throws Exception
    {
        Fraction fraction = UnitTestUtils.fractionBuilderByProfile("A").get(0);

        when(fractionService.getProfileFractionSum(fraction.getProfile())).thenReturn(new Double(2));

        doNothing().when(fractionService).addFraction(fraction);

        mockMvc.perform(
                put("/api/updateFraction/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(fraction)))
                .andExpect(status().isBadRequest())
                .andDo(print())
                .andReturn();
    }

    @Test public void deleteFraction() throws Exception
    {
        Fraction fraction = UnitTestUtils.fractionBuilderByProfile("A").get(0);

        when(fractionService.getFractionByProfileAndMonth("A", "JAN")).thenReturn(fraction);

        doNothing().when(fractionService).deleteFraction("A", "JAN");

        mockMvc.perform(
                delete("/api/deleteFraction/{profile}/{monthName}", fraction.getProfile(), fraction.getMonthName()))
                .andExpect(status().isOk())
                .andDo(print())
                .andReturn();
    }

}
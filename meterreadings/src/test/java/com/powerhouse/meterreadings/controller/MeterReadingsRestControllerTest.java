package com.powerhouse.meterreadings.controller;

import com.powerhouse.meterreadings.MeterreadingsApplication;
import com.powerhouse.meterreadings.Utils.UnitTestUtils;
import com.powerhouse.meterreadings.model.Fraction;
import com.powerhouse.meterreadings.model.MeterReading;
import com.powerhouse.meterreadings.service.FractionService;
import com.powerhouse.meterreadings.service.MeterReadingService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static com.powerhouse.meterreadings.Utils.UnitTestUtils.asJsonString;
import static org.mockito.Mockito.*;
import static org.mockito.internal.verification.VerificationModeFactory.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MeterreadingsApplication.class)
public class MeterReadingsRestControllerTest
{
    @MockBean
    private FractionService fractionService;

    @MockBean
    private MeterReadingService meterReadingService;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;


    @Before
    public void setUp() throws Exception
    {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void getAllMeterReadings() throws Exception
    {
        List<MeterReading> meterReadingList = UnitTestUtils.meterBuilderByAll();


        when(meterReadingService.getAllMeterReadings()).thenReturn(meterReadingList);

        mockMvc.perform(get("/api/meterReadings/"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andDo(print())
                .andReturn();


        verify(meterReadingService, times(1)).getAllMeterReadings();
        verifyNoMoreInteractions(meterReadingService);
    }

    @Test
    public void getAllMeterReadingsIsNotFound() throws Exception
    {
        List<MeterReading> meterReadingList = UnitTestUtils.meterBuilderByAll();

        when(meterReadingService.getAllMeterReadings()).thenReturn(meterReadingList);

        mockMvc.perform(get("/api/wronglink/"))
                .andExpect(status().isNotFound())
                .andDo(print())
                .andReturn();

    }

    @Test
    public void addMeterReading() throws Exception
    {
        MeterReading meterReading = UnitTestUtils.meterBuilderByProfile("A", "0001").get(0);
        List<Fraction> fractions = UnitTestUtils.fractionBuilderByProfile("A");

        when(meterReadingService.validateMeterReadingConsumption(meterReading)).thenReturn(false);
        when(fractionService.getFractionsByProfile(meterReading.getProfile())).thenReturn(fractions);

        doNothing().when(meterReadingService).addMeterReadings(meterReading);

        mockMvc.perform(
                post("/api/addMeterReading/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(meterReading)))
                .andExpect(status().isCreated())
                .andDo(print())
                .andReturn();

    }

    @Test
    public void addMeterReadingPreviousMonthValueControlFailed() throws Exception
    {
        MeterReading meterReading = UnitTestUtils.meterBuilderByProfile("A", "0001").get(0);

        when(meterReadingService.validateMeterReadingConsumption(meterReading)).thenReturn(true);

        doNothing().when(meterReadingService).addMeterReadings(meterReading);

        mockMvc.perform(
                post("/api/addMeterReading/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(meterReading)))
                .andExpect(status().isBadRequest())
                .andDo(print())
                .andReturn();
    }

    @Test
    public void addMeterReadingWithNoFractionProfileControlFailed() throws Exception
    {
        MeterReading meterReading = UnitTestUtils.meterBuilderByProfile("A", "0001").get(0);
        List<Fraction> fractions = new ArrayList<>();

        when(fractionService.getFractionsByProfile(meterReading.getProfile())).thenReturn(fractions);

        doNothing().when(meterReadingService).addMeterReadings(meterReading);

        mockMvc.perform(
                post("/api/addMeterReading/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(meterReading)))
                .andExpect(status().isBadRequest())
                .andDo(print())
                .andReturn();
    }

    @Test public void updateMeterReadingPreviousMonthValueControlFailed() throws Exception
    {
        MeterReading meterReading = UnitTestUtils.meterBuilderByProfile("A", "0001").get(0);

        when(meterReadingService.validateMeterReadingConsumption(meterReading)).thenReturn(true);

        doNothing().when(meterReadingService).updateMeterReadings(meterReading);

        mockMvc.perform(
                put("/api/updateMeterReading/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(meterReading)))
                .andExpect(status().isBadRequest())
                .andDo(print())
                .andReturn();

    }

    @Test public void updateMeterReadingWithNoFractionProfileControlFailed() throws Exception
    {
        MeterReading meterReading = UnitTestUtils.meterBuilderByProfile("A", "0001").get(0);
        List<Fraction> fractions = new ArrayList<>();

        when(fractionService.getFractionsByProfile(meterReading.getProfile())).thenReturn(fractions);

        doNothing().when(meterReadingService).updateMeterReadings(meterReading);

        mockMvc.perform(
                put("/api/updateMeterReading/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(meterReading)))
                .andExpect(status().isBadRequest())
                .andDo(print())
                .andReturn();

    }

    @Test public void updateMeterReading() throws Exception
    {
        MeterReading meterReading = UnitTestUtils.meterBuilderByProfile("A", "0001").get(0);
        List<Fraction> fractions = UnitTestUtils.fractionBuilderByProfile("A");

        when(meterReadingService.validateMeterReadingConsumption(meterReading)).thenReturn(false);
        when(fractionService.getFractionsByProfile(meterReading.getProfile())).thenReturn(fractions);

        doNothing().when(meterReadingService).updateMeterReadings(meterReading);

        mockMvc.perform(
                put("/api/updateMeterReading/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(meterReading)))
                .andExpect(status().isCreated())
                .andDo(print())
                .andReturn();

    }

    @Test public void deleteMeterReading() throws Exception
    {
        MeterReading meterReading = UnitTestUtils.meterBuilderByProfile("A", "0001").get(0);

        when(meterReadingService.getMeterReadingsByProfileAndMonth("A", "JAN")).thenReturn(meterReading);

        doNothing().when(meterReadingService).deleteMeterReadings("A", "JAN");

        mockMvc.perform(
                delete("/api/deleteMeterReading/{profile}/{month}", meterReading.getProfile(), meterReading.getMonth()))
                .andExpect(status().isOk())
                .andDo(print())
                .andReturn();

    }

    @Test public void validateAllMeterReadings() throws Exception
    {
        List<MeterReading> meterReadingList = UnitTestUtils.meterBuilderForValidation("A", "0001");

        when(meterReadingService.validateAllConsumption()).thenReturn(meterReadingList);

        mockMvc.perform(get("/api/validate/"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andDo(print())
                .andReturn();


        verify(meterReadingService, times(1)).validateAllConsumption();
        verifyNoMoreInteractions(meterReadingService);
    }

}